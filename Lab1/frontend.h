#pragma once

#include "struct_lexer.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;


int show(ofstream &fout1, ofstream &fout2, vector<Lexem> &lexem_tab, vector<Lexer_error> &error_tab, vector<Constant> &cons_tab, vector<Identifier> &ident_tab);
int lexer(ifstream &fin, vector<Lexem> &lexem_tab, vector<Lexer_error> &error_tab, vector<Constant> &cons_tab, vector<Identifier> &ident_tab);