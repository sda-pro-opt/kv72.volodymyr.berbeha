#pragma once

#include "struct_lexer.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;


int keyword_search(string tmp);
int constant_search(int constant);
int constant_tab(int constant);
int identifier_search(string tmp);
int identifier_tab(string tmp);
int lexer(ifstream &fin);

//int show();

Symbol get_symbol(ifstream &fin);


int show(ofstream &fout1, ofstream &fout2, vector<Lexem> &lexem_tab, vector<Lexer_error> &error_tab, vector<Constant> &cons_tab, vector<Identifier> &ident_tab);
int lexer(ifstream &fin, vector<Lexem> &lexem_tab, vector<Lexer_error> &error_tab, vector<Constant> &cons_tab, vector<Identifier> &ident_tab);




//void func1();