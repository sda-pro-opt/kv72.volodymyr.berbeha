#include "lexer.h"


int show(ofstream &fout1, ofstream &fout2, vector<Lexem> &lexem_tab, vector<Lexer_error> &error_tab, vector<Constant> &cons_tab, vector<Identifier> &ident_tab)
{
    
     for (Lexem lex : lexem_tab) 
     {
        if(lex.value.size() < 6)
        {
           cout<< "|" << lex.value << " \t\t| " << lex.code << " \t| " << lex.row << " \t| " << lex.column << " \t| " << endl;
           if(lex.value.size() > 1)
           {
               fout1<< "|" << lex.value << " \t\t| " << lex.code << " \t| " << lex.row << " \t| " << lex.column << " \t| " << endl;
           }
           else
           {
               fout1<< "|" << lex.value << " \t\t\t| " << lex.code << " \t| " << lex.row << " \t| " << lex.column << " \t| " << endl;
           }
           
           
        }
        else
        {
           cout<< "|" << lex.value << " \t| " << lex.code << " \t| " << lex.row << " \t| " << lex.column << " \t| " << endl;
           fout1<< "|" << lex.value << " \t| " << lex.code << " \t| " << lex.row << " \t| " << lex.column << " \t| " << endl;
        }
        fout2 << lex.code << " ";
    }

    for(Lexer_error lex_error : error_tab)
    {
        if (lex_error.type != 0)
        {
            cout << "Lexer Error: ";
            fout1 << "Lexer Error: ";
            if (lex_error.type == 50)
            {
                cout << "unclosed comment, " << "row " << lex_error.row << ", column " << lex_error.column << endl;
                fout1 << "unclosed comment, " << "row " << lex_error.row << ", column " << lex_error.column << endl; 
         
            }
            else 
            {
                if (lex_error.type == 60)
                {
                    cout << "Illegal symbol: " << lex_error.sym << ", " << "row " << lex_error.row << ", column " << lex_error.column <<  endl;
                    fout1 << "Illegal symbol: " << lex_error.sym << ", " << "row " << lex_error.row << ", column " << lex_error.column <<  endl;

                }
            }
        }
    }
    

    cout << endl << "Constant table:" << endl;
    fout1 << endl << "Constant table:" << endl;
    for(Constant cons : cons_tab)
    {
        cout << cons.value << " --- " << cons.code << endl;
        fout1 << cons.value << " --- " << cons.code << endl;
    }
    
    cout << endl << "Identifier table:" << endl;
    fout1 << endl << "Identifier table:" << endl;
    for(Identifier ident : ident_tab)
    {
        cout << ident.value << " --- " << ident.code << endl;
        fout1 << ident.value << " --- " << ident.code << endl;
    }
    
    return 0;
}