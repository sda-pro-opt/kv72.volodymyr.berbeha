
#include "frontend.h"

int main()
{
    vector<Lexem> lexem_tab;
    vector<Lexer_error> error_tab;
    vector<Constant> cons_tab;
    vector<Identifier> ident_tab;

    string test;
    cout << "Enter folder (TrueTest(1 or 2) or FalseTest(1 or 2)): " << endl;
    cin >> test;
    ifstream fin("Test/" + test + "/input.sig");
    ofstream fout1("Test/" + test + "/generated.txt");
    ofstream fout2("Test/" + test + "/expected.txt");
    if (fin.peek() == std::ifstream::traits_type::eof()) 
    {
        cout << "Empty file or no file" << endl;
        return 0;
    }

    lexer(fin, lexem_tab, error_tab, cons_tab, ident_tab);

    show(fout1, fout2, lexem_tab, error_tab, cons_tab, ident_tab);

    fin.close();
    fout1.close();
    fout2.close();
    return 0;
}